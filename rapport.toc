\select@language {french}
\contentsline {section}{Introduction}{2}
\contentsline {section}{\numberline {1}Environnement professionnel}{3}
\contentsline {subsection}{\numberline {1.1}Pr\'esentation de l'OSU-R\'eunion}{3}
\contentsline {subsection}{\numberline {1.2}Missions de l'OSU-R\'eunion}{3}
\contentsline {subsection}{\numberline {1.3}Structures}{4}
\contentsline {subsection}{\numberline {1.4}Positionnement \'economique}{4}
\contentsline {subsection}{\numberline {1.5}Service informatique}{4}
\contentsline {section}{\numberline {2}Le projet Licorne}{5}
\contentsline {subsection}{\numberline {2.1}Pr\'esentation}{5}
\contentsline {subsection}{\numberline {2.2}Objectifs}{6}
\contentsline {subsection}{\numberline {2.3}Description du framework}{6}
\contentsline {subsubsection}{\numberline {2.3.1}Librairie Miscbox}{7}
\contentsline {subsubsection}{\numberline {2.3.2}Librairie Lidata}{7}
\contentsline {subsubsection}{\numberline {2.3.3}Librairie Lidarcommons}{8}
\contentsline {subsubsection}{\numberline {2.3.4}Application Lisipro}{9}
\contentsline {subsection}{\numberline {2.4}\'Equipe projet}{9}
\contentsline {subsection}{\numberline {2.5}Objectifs du stage}{10}
\contentsline {section}{\numberline {3}Premi\`ere mission : Chef de projet}{11}
\contentsline {subsection}{\numberline {3.1}\'Etat de l'art}{11}
\contentsline {subsubsection}{\numberline {3.1.1}Gestion de projet}{12}
\contentsline {paragraph}{R\'epartition des r\^oles avec Scrum}{12}
\contentsline {paragraph}{Organisation du d\'eveloppement avec Scrum}{13}
\contentsline {subsubsection}{\numberline {3.1.2}G\'enie logiciel python}{13}
\contentsline {paragraph}{Structuration}{14}
\contentsline {paragraph}{Respect des r\`egles de codage}{14}
\contentsline {paragraph}{Robustesse}{15}
\contentsline {subsection}{\numberline {3.2}Chef de projet}{15}
\contentsline {subsubsection}{\numberline {3.2.1}Mon R\^ole}{15}
\contentsline {subsubsection}{\numberline {3.2.2}Documents r\'ealis\'es}{16}
\contentsline {subsection}{\numberline {3.3}Outils}{16}
\contentsline {subsubsection}{\numberline {3.3.1}Planning}{16}
\contentsline {subsubsection}{\numberline {3.3.2}Kanboard}{17}
\contentsline {subsection}{\numberline {3.4}Analyse Qualit\'e}{17}
\contentsline {subsubsection}{\numberline {3.4.1}Tests et int\'egration continue}{17}
\contentsline {paragraph}{nose}{18}
\contentsline {paragraph}{tox}{18}
\contentsline {paragraph}{Jenkins}{18}
\contentsline {subsubsection}{\numberline {3.4.2}Qualit\'e structurelle}{18}
\contentsline {paragraph}{flake8}{19}
\contentsline {paragraph}{coverage}{19}
\contentsline {paragraph}{pylint}{20}
\contentsline {paragraph}{radon}{20}
\contentsline {paragraph}{memory\_profiler}{20}
\contentsline {subsection}{\numberline {3.5}Recommandations}{21}
\contentsline {subsection}{\numberline {3.6}Site web}{21}
\contentsline {subsection}{\numberline {3.7}Analyse finale}{22}
\contentsline {section}{\numberline {4}Seconde mission : D\'eveloppeur Web-Python}{23}
\contentsline {subsection}{\numberline {4.1}Application Chain Editor}{24}
\contentsline {subsubsection}{\numberline {4.1.1}Description des fen\^etres}{25}
\contentsline {paragraph}{Fen\^etre "Controls"}{25}
\contentsline {paragraph}{Fen\^etre "Toolbox"}{25}
\contentsline {paragraph}{Fen\^etre "Viewer"}{25}
\contentsline {paragraph}{Fen\^etre "Configuration"}{25}
\contentsline {subsubsection}{\numberline {4.1.2}Description des technologies}{25}
\contentsline {paragraph}{Bootstrap}{26}
\contentsline {paragraph}{Flask}{27}
\contentsline {subsection}{\numberline {4.2}D\'emarche appliqu\'ee}{28}
\contentsline {subsubsection}{\numberline {4.2.1}L'interface graphique}{28}
\contentsline {subsubsection}{\numberline {4.2.2}Toolbox}{29}
\contentsline {subsubsection}{\numberline {4.2.3}Control}{29}
\contentsline {subsubsection}{\numberline {4.2.4}Viewer}{31}
\contentsline {paragraph}{Jstree}{31}
\contentsline {paragraph}{CodeMirror}{31}
\contentsline {paragraph}{XMLTree}{31}
\contentsline {paragraph}{JsonEditor}{31}
\contentsline {subsection}{\numberline {4.3}R\'esultats}{32}
\contentsline {subsection}{\numberline {4.4}Perspectives}{32}
\contentsline {section}{Conclusion}{33}
\contentsline {section}{R\'ef\'erences}{34}
\contentsline {section}{Acronymes}{35}
\contentsline {section}{Glossaire}{36}
\contentsline {section}{\numberline {A}Annexes}{37}
\contentsline {subsection}{\numberline {A.1}Plus d'informations sur OSU-Reunion}{37}
\contentsline {subsection}{\numberline {A.2}Planning \'Equipe}{38}
\contentsline {subsection}{\numberline {A.3}Planning personnel}{39}
\contentsline {paragraph}{Janvier}{39}
\contentsline {paragraph}{F\'evrier}{39}
\contentsline {paragraph}{Mars}{39}
\contentsline {paragraph}{Avril}{39}
\contentsline {paragraph}{Mai}{39}
\contentsline {paragraph}{Juin}{39}
\contentsline {subsection}{\numberline {A.4}Tutoriel d'installation de Licorne}{40}
\contentsline {subsubsection}{\numberline {A.4.1}Pr\'erequis}{40}
\contentsline {subsubsection}{\numberline {A.4.2}Installation}{40}
\contentsline {subsubsection}{\numberline {A.4.3}Utilisation}{40}
\contentsline {paragraph}{\'Ex\'ecuter une ch\IeC {\^\i }ne de traitement \\ \\}{40}
\contentsline {paragraph}{G\'en\'erer le fichier de configuration \\ \\}{40}
\contentsline {paragraph}{Lancer l'interface graphique \\ \\}{40}
\contentsline {subsection}{\numberline {A.5}Interface de Lisipro}{41}
\contentsline {subsection}{\numberline {A.6}Site web}{43}
